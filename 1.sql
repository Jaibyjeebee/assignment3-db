/* Query 1 : Determine the top 10 of players that have played against the highest number of other players.
Let's first create a column computing against how many opponents each player has played. 
    Participants have a player_id and a match_id, such that for any match_id we might select all distinct participants and add them to the sum of each of those
We will then ORDER BY cnt DESC LIMIT 10 
*/


/* Temp gives us the number of players for each match, with its id
    Temp2 gives us the number of matches for each player, with its id */
/* We then select the id of the selection that took the top 10 of the substractions of the number of players in the matches one player took part in with the number of matches this player has played (because he was counted in the number of players having played the matches) */
WITH temp AS (SELECT COUNT(*) as cnt, match_id as id FROM (SELECT DISTINCT * FROM Participants) GROUP BY match_id),
temp2 AS (SELECT COUNT(*) as cnt2, player_id as id FROM (SELECT DISTINCT * FROM Match, Participants WHERE Match.match_id = Participants.match_id) GROUP BY player_id) 
SELECT id FROM (SELECT SUM(cnt)-cnt2 as cnt, player_id as id FROM temp, temp2, Participants WHERE temp.id = Participants.match_id AND temp2.id = Participants.player_id GROUP BY Participants.player_id ORDER BY cnt DESC LIMIT 10)

