/* Query 2 : Count the number of pairs of players that played in more than 200 matches, but that never
played against each other. */

/* Let's first see who are the players having played more than 200 matches
Let's see then what pairs of players have played against each other
We then subtract the second from a JOIN of two times the first
*/

/* There are 183 players with more than 200 matches*/

WITH 
temp AS (SELECT COUNT(*) as cnt, player_id as id FROM (SELECT DISTINCT * FROM Match, Participants WHERE Match.match_id = Participants.match_id) GROUP BY player_id),
players AS (SELECT id FROM temp WHERE cnt > 200)
SELECT id as id1, id as id2 FROM (players TIMES players)

SELECT DISTINCT P1.player_id as id1, P2.player_id as id2 FROM Participants as P1, Participants as P2 WHERE P1.match_id = P2.match_id AND P1.player_id = players.id AND P2.player_id = players.id

pairPlayers AS (SELECT DISTINCT players.player_id as id1, players.player_id as id2 FROM Participants as P1, Participants as P2 WHERE P1.match_id = P2.match_id AND P1.player_id = players.player_id AND P2.players_id = players.player_id )
SELECT COUNT(*) FROM (SELECT DISTINCT id1 FROM pairPlayers)
